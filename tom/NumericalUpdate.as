﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import fl.motion.Color;
	
	public class NumericalUpdate extends MovieClip{
		// How many frames this update will last before being destroyed and how many pixels it will float
		//	upwards for in that lifetime.
		public var	lifeTime:int, floatHeight:int,
		// Values measuring exactly how much needs to be changed per frame for the above to work correctly.
					lifeLostPerFrame:Number, heightGainedPerFrame:Number;
		// Constructor.
		public function NumericalUpdate(X:int, Y:int, valueDisplayed:Number, color:uint):void{
			lifeTime = 60.0; lifeLostPerFrame = 1.0/lifeTime;
			floatHeight = 50.0; heightGainedPerFrame = floatHeight/lifeTime;
			
			this.x = X; this.y = Y
			// Display the given value.
			this.num.text = ""+valueDisplayed;
			// Display in the given color.
			this.num.textColor = color;
			
			// Add the listener allowing this component to update.
			addEventListener(Event.ENTER_FRAME, loop, false, 0, true);
		}
		public function loop(e:Event):void{
			// Lose visibility as this update fades away.
			alpha -= lifeLostPerFrame;
			// Destroy this update when it is no longer visible.
			if(alpha <= 0){ destroy(); }
			// Float up onscreen.
			y -= heightGainedPerFrame;
		}
		// Please set all refernces to this object that may exist to null after calling this function.
		public function destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, loop, false);
			this.stop();
		}
	}
}