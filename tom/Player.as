﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import KeyObject;
	import flash.net.drm.VoucherAccessInfo;
	
	public class Player extends MovieClip{
		// References to the game 'stage' and the main game manager.
		public var	stageRef:Stage, mainRef:Main,
		// A reference to the player's 'PlayerFront' object. This object will create a rectangular area in front
		//	of the player, where things like projectiles can then be generated. 'Front' refers to this player's
		//	rotation, where an angle of zero has the front facing right.
					front:PlayerFront,
					key:KeyObject,
					
		// The player's location in the previous frame. Primarily used for collisions.
					prevX:int, prevY:int,
		// The current angle the player is facing, zero being to the right and going counter-clockwise.
					angle:Number,
		// A boolean keeping track of whether or not this game is paused.
					pauseGame:Boolean,
		
		// Boolean variables that track whether or not their corresponding keyboard keys are currently pressed.
					leftPressed:Boolean, rightPressed:Boolean,
					upPressed:Boolean, downPressed:Boolean,
					spacePressed:Boolean,
					rReleased:Boolean, pReleased:Boolean, mReleased:Boolean, qReleased:Boolean,
		// The player's acceleration and movement speed (in pixels per frame).
					xSpeed:Number, ySpeed:Number,
					moveSpeedMax:Number, sprintSpeedMax:Number, climbSpeedMax:Number, dropSpeedMax:Number,
					moveAcc:Number, sprintAcc:Number, climbAcc:Number, dropAcc:Number,
		// Cooldown time for the player's attacks. Time is measured in frames.
					cooldownLaser:int, cooldownMissile:int,
		// Rate of fire for the player's attacks. Lower ROF values mean less time between attacks.
					laserROF:int,  missileROF:int,
		
		// The location and size of the game screen the player is allowed to move in.
					screenX:int, screenY:int,
					screenWidth:int, screenHeight:int,
		// Half the player's width and height. Saving this saves on math.
					halfWidth:Number, halfHeight:Number;
		
		// Constructor.
		public function Player(stageRef_:Stage, mainRef_:Main):void{
			this.stageRef = stageRef_;
			this.mainRef = mainRef_;
			
			key = new KeyObject(stageRef);
			this.front = new PlayerFront(x, y);
			stageRef.addChild(front);
			
			angle = 0.0;
			restart();
			
			// Add the listener allowing this component to update.
			addEventListener(Event.ENTER_FRAME, loop, false, 0, true);
			// Capture clicks on the stage.
			stageRef.addEventListener(MouseEvent.CLICK, mouseClicked);
		}
		public function restart():void{
			prevX = 0; prevY = 0;
			this.visible = true;
			pauseGame = false;
			
			halfWidth = width*0.5;
			halfHeight = height*0.5;
			
			leftPressed = false; rightPressed = false;
			upPressed = false; downPressed = false;
			spacePressed = false;
			rReleased = false; pReleased = false; mReleased = false; qReleased = false;
			
			xSpeed = 0; ySpeed = 0;
			moveSpeedMax = 6; sprintSpeedMax = 12; climbSpeedMax = 3.2; dropSpeedMax = 7;
			// NOTE: Sprint acceleration stacks with move acceleration.
			moveAcc = 0.12; sprintAcc = 0.03; climbAcc = 0.12; dropAcc = 0.24;
			
			cooldownLaser = 0; cooldownMissile = 0;
			laserROF = 10; missileROF = 30;
			
			screenX = 0; screenY = 100;
			screenWidth = 800; screenHeight = 500;
			
			// This overcomes the safety check preventing rotation to the current angle.
			rotate(180); rotate(0);
			this.gotoAndStop("still");
		}
		public function loop(e:Event):void{
			// Check for and execute pausing, restarting, and other such UI commands.
			UIControls();
			
			if(!visible || pauseGame){ return; }
			prevX = x; prevY = y;
			// Make the player show in front of everything else.
			front.parent.setChildIndex(front, front.parent.numChildren-1);
			this.parent.setChildIndex(this, this.parent.numChildren-1);
			
			shiftPlayer(xSpeed, ySpeed);
			// Check for player input to control the player character.
			checkPlayerControls();
			// Move the player according to arrow key presses.
			applyPlayerMovement();
			
			--cooldownLaser; if(cooldownLaser < 0){ cooldownLaser = 0; }
			--cooldownMissile; if(cooldownMissile < 0){ cooldownMissile = 0; }
			if(spacePressed && cooldownLaser <= 0){
				cooldownLaser = laserROF;
				var newLaser = new Entity(mainRef, front.x, front.y, 0, 0, "playerLaser", 120, true);
				mainRef.currentEntities.push(newLaser); stage.addChild(newLaser);
			}
			
			// Stop the player from moving off-screen.
			// Stop the player from going above the stage.
			if(y < (screenY + halfHeight)){
				movePlayerY(screenY + halfHeight); ySpeed = 0;
			}
			// Stop the player from going below the stage.
			if(y > (screenY + screenHeight - halfHeight)){
				movePlayerY(screenY + screenHeight - halfHeight); ySpeed = 0;
			}
			// Stop the player from going left of the stage.
			if(x < (screenX + halfWidth)){
				movePlayerX(screenX + halfWidth); xSpeed = 0;
			}
			// Stop the player from going right of the stage.
			if(x > (screenX + screenWidth - halfWidth)){
				movePlayerX(screenX + screenWidth - halfWidth); xSpeed = 0;
			}
		}
		protected function mouseClicked(event:MouseEvent):void{
			if(!visible || pauseGame){ return; }
			if(cooldownMissile <= 0){
				cooldownMissile = missileROF;
				var newMissile = new Entity(mainRef, x, y, xSpeed, ySpeed, "playerMissile", 300, true);
				mainRef.currentEntities.push(newMissile); stage.addChild(newMissile);
			}
		}
		private function UIControls():void{
			// Check if the 'Q' key is pressed: if it is, go to the main menu.
			if(qReleased && key.isDown(81)){
				qReleased = false;
				mainRef.goToMainMenu();
				pauseGame = true;
			}
			else if(!key.isDown(81)){ qReleased = true; }
			// Check if the 'M' key is pressed: if it is, mute/unmute the game.
			if(mReleased && key.isDown(77)){ mReleased = false; mainRef.toggleMusic(); }
			else if(!key.isDown(77)){ mReleased = true; }
			// Do nothing in this class after this point if in the menu.
			if(mainRef.mainMenu.visible){ return; }
			
			// Check if the 'R' key is pressed: if it is, restart the game.
			if(rReleased && key.isDown(82)){ rReleased = false; mainRef.restart(); }
			else if(!key.isDown(82)){ rReleased = true; }
			// Check if the 'P' key is pressed: if it is, pause/unpause the game.
			if(pReleased && key.isDown(80)){ pReleased = false; pauseGame = !pauseGame; }
			else if(!key.isDown(80)){ pReleased = true; }
		}
		private function checkPlayerControls():void{
			// Check if the left arrow key or 'A' are pressed:
			if(key.isDown(37) || key.isDown(65)){ leftPressed = true; }
			else{ leftPressed = false; }
			// Check if the up arrow key or 'W' are pressed:
			if(key.isDown(38) || key.isDown(87)){ upPressed = true; }
			else{ upPressed = false; }
			// Check if the right arrow key or 'D' are pressed:
			if(key.isDown(39) || key.isDown(68)){ rightPressed = true; }
			else{ rightPressed = false; }
			// Check if the down arrow key or 'S' are pressed:
			if(key.isDown(40) || key.isDown(83)){ downPressed = true; }
			else{ downPressed = false; }
			// Check if the spacebar key is pressed:
			if(key.isDown(32)){ spacePressed = true; }
			else{ spacePressed = false; }
		}
		private function applyPlayerMovement():void{
			if(upPressed){
				this.gotoAndStop("still");
				ySpeed -= climbAcc;
				// This helps climb faster when stopping a fall.
				if(Math.abs(ySpeed) < 0.5){ ySpeed -= climbAcc; }
				if(ySpeed < -climbSpeedMax){ ySpeed = -climbSpeedMax; }
				rotate(Math.PI*0.5);
			}
			else if(downPressed){
				this.gotoAndStop("still");
				ySpeed += dropAcc;
				if(ySpeed > dropSpeedMax){ ySpeed = dropSpeedMax; }
				rotate(Math.PI*1.5);
			}
			else{
				if(ySpeed > 0){ ySpeed -= dropAcc; if(ySpeed < 0){ ySpeed = 0; } }
				else if(ySpeed < 0){ ySpeed += climbAcc; if(ySpeed > 0){ ySpeed = 0; } }
			}
			if(leftPressed){
				this.gotoAndStop("moveLeft");
				xSpeed -= moveAcc;
				if(xSpeed < -moveSpeedMax){
					if(!spacePressed){
						this.gotoAndStop("sprintLeft");
						xSpeed -= sprintAcc;
						if(xSpeed < -sprintSpeedMax){ xSpeed = -sprintSpeedMax; }
					}
					else{ xSpeed = -moveSpeedMax; }
				}
				rotate(Math.PI);
			}
			else if(rightPressed){
				this.gotoAndStop("moveRight");
				xSpeed += moveAcc;
				if(xSpeed > moveSpeedMax){
					if(!spacePressed){
						this.gotoAndStop("sprintRight");
						xSpeed += sprintAcc;
						if(xSpeed > sprintSpeedMax){ xSpeed = sprintSpeedMax; }
					}
					else{ xSpeed = moveSpeedMax; }
				}
				rotate(0);
			}
			else{
				if(xSpeed > 0){ xSpeed -= moveAcc; if(xSpeed < 0){ xSpeed = 0; } }
				else if(xSpeed < 0){ xSpeed += moveAcc; if(xSpeed > 0){ xSpeed = 0; } }
			}
			// Rotate correctly for diagonal movement.
			if(upPressed){
				if(rightPressed){ rotate(Math.PI*0.25); }
				else if(leftPressed){ rotate(Math.PI*0.75); }
			}
			else if(downPressed){
				if(rightPressed){ rotate(Math.PI*1.75); }
				else if(leftPressed){ rotate(Math.PI*1.25); }
			}
			// Set player animation to idle if they have not moved.
			if(!leftPressed && !rightPressed && !upPressed && !downPressed){
				this.gotoAndStop("still");
			}
		}
		public function collideWith(other:Entity):void{
			switch(other.ID){
				case "wallSquare":
				case "wallTriangle":
					movePlayer(prevX, prevY);
					break;
				case "explosion":
					// Hurt/kill player.
					break;
				default:
					break;
			}
		}
		public function setDifficulty(difficulty:String):void{
			switch(difficulty){
				case "easy":
					break;
				case "normal":
					break;
				case "hard":
					break;
				case "nightmare":
					break;
				default:
					break;
			}
		}
		public function rotate(newAngle:Number):void{
			if(angle == newAngle){ return; }
			angle = newAngle;
			var angleDegrees:int = angle*mainRef.RAD_TO_DEGREES;
			
			front.x = x + halfWidth*Math.cos(angle); front.y = y - halfHeight*Math.sin(angle);
			front.rotation = 90 - angleDegrees;
			
			front.stretch(0.9*(halfWidth*Math.abs(Math.sin(angle)) + halfHeight*Math.abs(Math.cos(angle))), 10);
		}
		// Changes the player's width and height dimensions to those given.
		public function stretch(sizeX:int, sizeY:int):void{
			width = sizeX; height = sizeY;
			halfWidth = width*0.5; halfHeight = height*0.5;
			front.stretch(0.9*(Math.abs(halfWidth*Math.sin(angle)) + Math.abs(halfHeight*Math.cos(angle))), 10);
		}
		public function shiftPlayer(X:int, Y:int):void{
			x += X; y += Y;
			front.x += X; front.y += Y;
		}
		public function shiftPlayerX(X:int):void{ shiftPlayer(X, 0); }
		public function shiftPlayerY(Y:int):void{ shiftPlayer(0, Y); }
		public function movePlayer(X:int, Y:int):void{ shiftPlayer(X - x, Y - y); }
		public function movePlayerX(X:int):void{ shiftPlayer(X - x, 0); }
		public function movePlayerY(Y:int):void{ shiftPlayer(0, Y - y); }
	}
}