﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.ActivityEvent;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest; 
	
	public class Main extends MovieClip{
		public var	RAD_TO_DEGREES:Number = 180/Math.PI,
					DEGREES_TO_RAD:Number = Math.PI/180;
		public var	player:Player, mainMenu:MainMenu,
					soundChannel:SoundChannel, soundOn:Boolean,
					backgroundMusic:Sound, menuMusic:Sound,
		// A vector containing the entities currently onscreen.
					currentEntities:Vector.<Entity>;
		
		// Constructor.
		public function Main():void{
			soundOn = false;
			// Create the menu music.
			menuMusic = new Sound();
			menuMusic = new Beep();
			// Create the background music.
			backgroundMusic = new Sound();
			backgroundMusic = new Beep();
			
			currentEntities = new Vector.<Entity>;
			
			player = new Player(stage, this);
			stage.addChild(player);
			
			mainMenu = new MainMenu(this);
			stage.addChild(mainMenu);
			mainMenu.x = 400; mainMenu.y = 300;
			
			restart();
			goToMainMenu();
		}
		public function restart():void{
			if(!mainMenu.visible){ restartMusic(); }
			player.x = 400; player.y = 350;
			
			// Destroy all current entities.
			for(var i:int = 0; i < currentEntities.length; ++i){ currentEntities[i].lifeTime = 0; }
			var target = new Entity(this, 700, 300, 0, 0, "target", -1, false);
			currentEntities.push(target); stage.addChild(target);
			target = new Entity(this, 200, 150, 0, 0, "target", -1, false);
			currentEntities.push(target); stage.addChild(target);
			target = new Entity(this, 400, 450, 0, 0, "target", -1, false);
			currentEntities.push(target); stage.addChild(target);
			
			player.restart();
		}
		public function goToMainMenu():void{
			mainMenu.visible = true;
			restartMusic();
		}
		
		// Play the background music.
		public function toggleMusic():void{
			if(soundOn){
				soundChannel.stop();
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, musicEnd, false);
			}
			else{
				playMusic();
				soundChannel.addEventListener(Event.SOUND_COMPLETE, musicEnd, false, 0, true);
			}
			soundOn = !soundOn;
		}
		public function musicEnd(e:Event):void{
			playMusic();
			soundChannel.addEventListener(Event.SOUND_COMPLETE, musicEnd, false, 0, true);
		}
		public function restartMusic():void{
			toggleMusic();
			if(!soundOn){ toggleMusic(); }
		}
		private function playMusic():void{
			if(mainMenu.visible){ soundChannel = menuMusic.play(); }
			else{ soundChannel = backgroundMusic.play(); }
		}
	}
}