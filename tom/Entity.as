﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.display.Graphics;
	
	public class Entity extends MovieClip{
		// Half this hand's width and height. Saving this saves on math.
		public var	halfWidth:Number, halfHeight:Number,
		// A reference to the main game manager and an ID defining which entity this is.
					mainRef:Main, ID:String,
		// How many frames this entity has left to exist before it is destroyed. A negative value means it exists indefinitely.
					lifeTime:int, initialLifeTime:int,
		
		// This entity's location in the previous frame. Primarily used for collisions.
					prevX:int, prevY:int,
		// The current angle this entity is facing, zero being to the right and going counter-clockwise.
					angle:Number,
		
		// This entity's movement speed and acceleration (in pixels per frame).
					xSpeed:Number, ySpeed:Number,
					moveSpeedMax:Number, climbSpeedMax:Number, dropSpeedMax:Number,
					moveAcc:Number, climbAcc:Number, dropAcc:Number,
		// Can the player collide with this entity?
					collisionEnabled:Boolean = true,
		// A vector containing the objects this entity can collide and interact with.
					targets:Vector.<MovieClip>;
		// Constructor.
		public function Entity(mainRef_:Main, X:int, Y:int, speedX:Number, speedY:Number,
					ID_:String, lifeTime_:int = -1, collidible:Boolean = true):void{
			this.mainRef = mainRef_;
			
			this.x = X; this.y = Y;
			prevX = 0; prevY = 0;
			this.collisionEnabled = collidible;
			this.ID = ID_;
			this.lifeTime = lifeTime_;
			this.initialLifeTime = lifeTime;
			
			this.xSpeed = speedX; this.ySpeed = speedY;
			moveSpeedMax = 0.0; climbSpeedMax = 0.0; dropSpeedMax = 7.0;
			moveAcc = 0.0; climbAcc = 0.0; dropAcc = 0.24;
			
			angle = 0;
			targets = new Vector.<MovieClip>;
			
			// Add the listener allowing this component to update.
			addEventListener(Event.ENTER_FRAME, loop, false, 0, true);
			// Make this object look like its supposed to.
			this.gotoAndStop(ID);
			this.visible = true;
			
			// Initialize this specific entity.
			switch(ID){
				case "wallSquare":
				case "wallTriangle":
					targets.push(mainRef.player);
					targetAll("playerLaser");
					targetAll("playerMissile");
					break;
				case "explosion":
					targets.push(mainRef.player);
					targetAll("playerMissile");
				case "target":
					break;
				case "playerLaser":
					rotate(mainRef.player.angle);
					moveSpeedMax = 20.0; climbSpeedMax = 0.0; dropSpeedMax = 0.0;
					moveAcc = 20.0; climbAcc = 0.0; dropAcc = 0.0;
					break;
				case "playerMissile":
					if(!targetClosest("target")){ explode(); }
					moveSpeedMax = 10.0; climbSpeedMax = 8.0; dropSpeedMax = 2.0;
					moveAcc = 0.7; climbAcc = 0.7; dropAcc = 0.4;
					break;
				default:
					break;
			}
		}
		public function loop(e:Event):void{
			if(!visible || mainRef.player.pauseGame){ return; }
			prevX = x; prevY = y;
			// If applicable, decrement this entity's remaining lifeTime until it is zero,
			//	at which point this entity is destroyed.
			if(lifeTime == 0){
				annihilate();
				return;
			}
			if(lifeTime > -999999999){ --lifeTime; }
			
			// Move this entity according to its speed.
			x += xSpeed; y -= ySpeed;
			// Collide with appropriate targets.
			for(var i:int = 0; i < targets.length; ++i){
				if(collisionEnabled && targets[i].visible && verifyCollision(targets[i])){
					targets[i].collideWith(this);
				}
			}
			// Cause effects specific to this kind of entity.
			effect();
		}
		// Cause this entity's unique effect.
		public function effect():void{
			switch(ID){
				case "wallSquare":
				case "wallTriangle":
					break;
				case "explosion":
					break;
				case "target":
					break;
				case "playerLaser":
					// Move in the direction it is facing.
					xSpeed = moveAcc*Math.cos(angle);
					ySpeed = moveAcc*Math.sin(angle);
					break;
				case "playerMissile":
					// Perfectly home in on its first target if at least half a second has passed
					//	since this missile's creation.
					if(lifeTime < (initialLifeTime - 30)){
						var	distX:Number = targets[0].x - x, distY:Number = targets[0].y - y,
							distLength:Number = Math.pow(distX*distX + distY*distY, 0.5);
						distX /= distLength; distY /= distLength;
						rotate(Math.acos(distX));
						if(Math.asin(distY) > 0){ rotate(-angle); }
						
						xSpeed += moveAcc*distX;
						if(xSpeed > moveSpeedMax){ xSpeed = moveSpeedMax; }
						else if(xSpeed < -moveSpeedMax){ xSpeed = -moveSpeedMax; }
						ySpeed -= climbAcc*distY;
						if(ySpeed > climbSpeedMax){ ySpeed = climbSpeedMax; }
						else if(ySpeed < -climbSpeedMax){ ySpeed = -climbSpeedMax; }
					}
					// Otherwise drop.
					else{
						ySpeed -= dropAcc;
						if(ySpeed > dropSpeedMax){ ySpeed = dropSpeedMax; }
						else if(ySpeed < -dropSpeedMax){ ySpeed = -dropSpeedMax; }
					}
					if(verifyCollision(targets[0])){ explode(); return; }
					break;
				default:
					break;
			}
		}
		public function annihilate():void{
			this.visible = false;
			this.removeEventListener(Event.ENTER_FRAME, loop, false);
			this.parent.removeChild(this);
			var temp:Vector.<Entity> = mainRef.currentEntities;
			temp[temp.indexOf(this)] = temp[temp.length-1]; temp.pop();
		}
		public function explode():void{
			lifeTime = 0;
			var explosion = new Entity(mainRef, x, y, 0, 0, "explosion", 40, true);
			mainRef.currentEntities.push(explosion); mainRef.stage.addChild(explosion);
			annihilate();
		}
		private function targetAll(entityType:String):void{
			var	entity:Entity;
			for(var i:int = 0; i < mainRef.currentEntities.length; ++i){
				entity = mainRef.currentEntities[i];
				if((entity is Entity) && (entity != this) && (entity.ID == entityType)){
					// Target only entities that are not already targeted.
					if(targets.indexOf(entity) == -1){ targets.push(entity); }
				}
			}
		}
		// This ignores already targeted entities.
		private function targetClosest(entityType:String):Boolean{
			var	entity:Entity, closestTarget:Entity = this,
				distX:Number = 0.0, distY:Number = 0.0,
				distLengthSquaredPrev:Number = 999999999.0, distLengthSquared:Number = 0.0;
			for(var i:int = 0; i < mainRef.currentEntities.length; ++i){
				entity = mainRef.currentEntities[i];
				if((entity is Entity) && (entity != this) && (entity.ID == entityType)){
					// Target only entities that are not already targeted.
					if(targets.indexOf(entity) != -1){ continue; }
					
					distX = entity.x - x; distY = entity.y - y;
					distLengthSquared = distX*distX + distY*distY;
					if(distLengthSquared < distLengthSquaredPrev){
						closestTarget = entity;
						distLengthSquaredPrev = distLengthSquared;
					}
				}
			}
			if(closestTarget == this){ return false; }
			targets.push(closestTarget); return true;
		}
		public function collideWith(other:Entity):void{
			switch(other.ID){
				case "wallSquare":
				case "wallTriangle":
					if(ID == "playerLaser"){ lifeTime = 0; }
					if(ID == "playerMissile"){ explode(); }
					break;
				case "explosion":
					if(ID == "playerMissile"){ explode(); }
					break;
				case "playerLaser":
					break;
				case "playerMissile":
					break;
				default:
					break;
			}
		}
		private function verifyCollision(other:MovieClip):Boolean{
			if(!other.hitTestObject(this)){ return false; }
			for(var i:int = 0; i < this.numChildren; ++i){
				if(this.getChildAt(i) is Hitbox){} else{ continue; }
				for(var j:int = 0; j < other.numChildren; ++j){
					if(other.getChildAt(j) is Hitbox){} else{ continue; }
					if(other.getChildAt(j).hitTestObject(this.getChildAt(i))){
						return true;
					}
				}
			}
			return false;
		}
		public function stretch(sizeX:int, sizeY:int):void{
			width = sizeX; height = sizeY;
			halfWidth = width*0.5;
			halfHeight = height*0.5;
		}
		public function rotate(newAngle:Number):void{
			if(angle == newAngle){ return; }
			angle = newAngle; this.rotation = -angle*mainRef.RAD_TO_DEGREES;
		}
		public function rand(min:Number, max:Number):Number{
			var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
			return randomNum;
		}
	}
}