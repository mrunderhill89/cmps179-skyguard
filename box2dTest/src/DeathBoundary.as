﻿package{
	import flash.events.Event;
	import Box2D.Common.Math.b2Vec2;
	
	public class DeathBoundary extends Component{
		/*
			Determines the maximum movement allowed for this object.
			If max < min for either x or y, that dimension is ignored.
		*/
		public var minX:Number, maxX:Number, minY:Number, maxY:Number;
		
		public function DeathBoundary(_minX:Number = 0, _maxX:Number = -1, _minY:Number = 0, _maxY:Number = -1){
			super();
			this.minX = _minX; this.maxX = _maxX;
			this.minY = _minY; this.maxY = _maxY;
			stageEvents[Main.UPDATE_EVENT] = this.update;
		}
		public function isCheckingX():Boolean{
			return maxX >= minX;
		}
		public function isCheckingY():Boolean{
			return maxY >= minY;
		}
		public function update(e:Event = null):void{
			var x:Number = parent.body.GetPosition().x * Main.PIXELS_TO_METER;
			var y:Number = parent.body.GetPosition().y * Main.PIXELS_TO_METER;
			var xc:Boolean = false, yc:Boolean = false;
			if(isCheckingX()){
				//Is the entity already past a boundary?
				if(x < minX || x > maxX){
					xc = true;
				}
			}
			if(isCheckingY()){
				//Is the entity already past a boundary?
				if(y < minY || y > maxY){
					yc = true;
				}				
			}
			if(xc || yc){
				parent.destroy();
			}
		}
	}
}
