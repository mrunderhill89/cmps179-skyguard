﻿package  {
	import flash.media.Sound;
	import flash.events.Event;
	
	public class SoundComponent extends Component{
		public var sound:Sound = null;
		public function SoundComponent(_s:Sound, onPlay:String = "", onStop:String = "", onLoop:String = "") {
			sound = _s;
			if (onPlay != "")
				events[onPlay] = this.play;
			if (onStop != "")
				events[onStop] = this.stop;
			if (onLoop != "")
				events[onLoop] = this.loop;
		}
		public function play(e:Event = null):void{
			sound.play();
		}
		public function stop(e:Event = null):void{
			//sound.stop();
		}
		public function loop(e:Event = null):void{
			//sound.loop();
		}
	}
	
}
