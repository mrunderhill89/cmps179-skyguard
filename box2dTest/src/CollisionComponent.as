﻿package  {
	import CollisionEvent;
	public class CollisionComponent extends Component{
		public var group:String = ""; //By default, activates when colliding with anything.
		public var onCollide:Function;
		public function CollisionComponent(_g:String = "", _oC:Function = null) {
			super();
			group = _g;
			onCollide = this.detectCollision;
			if (_oC != null)
				onCollide = _oC;
			events[CollisionEvent.TYPE] = this.checkGroup;
		}
		public function checkGroup(ce:CollisionEvent){
			if (group == "" || group == ce.that.collisionGroup){
				this.onCollide(ce);
			}
		}
		public function detectCollision(ce:CollisionEvent = null){
			trace("Collision detected!");
		}
		public function destroyParent(ce:CollisionEvent = null):void{
			trace("Collision detected. Deleting object...");
			parent.destroy();
		}
	}
	
}
