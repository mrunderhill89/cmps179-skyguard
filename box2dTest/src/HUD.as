﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class HUD extends MovieClip{
		// References to the main game manager and the player.
		public var	mainRef:Main, player:Player,
		
		// An object tracking the current state of the keyboard keys.
					key:KeyObject = null,
		// Boolean variables that track whether or not their corresponding keyboard keys are currently pressed.
					rReleased:Boolean = true, pReleased:Boolean = true,
					mReleased:Boolean = true, qReleased:Boolean = true;
		
		// Constructor.
		public function HUD(mainRef_:Main):void{
			this.mainRef = mainRef_;
			this.player = (mainRef.player as Player);
			
			this.gotoAndStop("UI");
			
			key = mainRef.key;
			// Add the listener allowing this component to update.
			addEventListener(Event.ENTER_FRAME, update);
			// Capture clicks on the stage.
			addEventListener(MouseEvent.CLICK, mouseClicked);
		}
		public function update(e:Event):void{
			if(!this.visible){ return; }
			//this.parent.stage.setChildIndex(this, this.parent.stage.numChildren-1);
			
			// Check if the 'Q' key is pressed: if it is, go to the main menu.
			if(qReleased && key.isDown(81)){
				qReleased = false;
				exitToMainMenu();
				return;
			}
			else if(!key.isDown(81)){ qReleased = true; }
			// Check if the 'M' key is pressed: if it is, mute/unmute the game.
			if(mReleased && key.isDown(77)){ mReleased = false; mainRef.toggleMute(); }
			else if(!key.isDown(77)){ mReleased = true; }
			
			// Check if the 'R' key is pressed: if it is, restart the game.
			if(rReleased && key.isDown(82)){ rReleased = false; mainRef.restart(); }
			else if(!key.isDown(82)){ rReleased = true; }
			// Check if the 'P' key is pressed: if it is, pause/unpause the game.
			if(pReleased && key.isDown(80)){ pReleased = false; mainRef.togglePaused(); }
			else if(!key.isDown(80)){ pReleased = true; }
			
			// Update the HUD components in the UI.
			this.score.text = ""+GameSettings.score;
			this.lives.text = ""+player.lives;
			this.missiles.text = ""+player.missiles;
			
			this.city1.text = "%"+City.cities[0].getHealth();
			this.city1.textColor = healthColor(City.cities[0].getHealth());
			this.city2.text = "%"+City.cities[1].getHealth();
			this.city2.textColor = healthColor(City.cities[1].getHealth());
			this.city3.text = "%"+City.cities[2].getHealth();
			this.city3.textColor = healthColor(City.cities[2].getHealth());
		}
		protected function mouseClicked(event:MouseEvent):void{}
		// Returns the appropriate color to display the given amount of health.
		public function healthColor(hp:int):uint{
			if(hp == 100){ return 0x00FF00; }
			else if(hp > 75){ return 0x00FF80; }
			else if(hp > 50){ return 0xCCFF00; }
			else if(hp > 25){ return 0xCC9900; }
			else if(hp > 0){ return 0xCC0000; }
			else if(hp == 0){ return 0xFF0000; }
			// If you got here, the value given was invalid.
			return null;
		}
		// Exits the current game, returning to the main menu.
		public function exitToMainMenu():void{
			mainRef.menu.visible = true;
			mainRef.pauseGame();
			this.visible = false;
		}
	}
}