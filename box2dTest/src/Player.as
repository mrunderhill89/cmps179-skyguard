﻿package{
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Player extends Entity{
		public var	lives:int = 0, missiles:int = 0,
					health:HealthComponent;
		
		public function Player(w:b2World, p:Sprite, x:Number = 400, y:Number = 400){
			super(w, p);
			createBoxBody(x, y, 15, 15, 1.0, 0.3, true, false);
			setOffset(0, 0);
			addComponent(new PlayerControls());
			addComponent(new PlayerCamera());
			addComponent(new PlayerWeapons());
			addComponent(new SoundComponent(new GunSound(), "fire"));
			addComponent(new SoundComponent(new AlarmSound(), "newWave"));
			addComponent(new MovementBoundary(-1600, 1600, 120, 600));
			addComponent(new RadarTag(0xFF00FF00, 5.0, 5.0));
			health = new HealthComponent(1, null, die);
			this.addComponent(health);
		}
		public function die(de:DamageEvent = null):void{
			if(dead){ return; }
			--lives; dead = true;
			health.health = health.maxHealth;
			if(lives <= 0){
				var mainRef:Main = Main.getInstance();
				mainRef.hud.exitToMainMenu();
				mainRef.menu.changeScreen(MainMenu.GAME_OVER);
			}
		}
		public override function reset(e:Event = null):void{
			this.body.SetPosition(new b2Vec2(400 * Main.METERS_TO_PIXEL, 400 * Main.METERS_TO_PIXEL));
			this.lives = 10;
			this.missiles = 42;
		}
	}
}