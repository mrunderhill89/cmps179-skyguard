﻿package{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MainMenu extends MovieClip{
		public static const MAIN_MENU:int = 0, GAME_OPTIONS:int = 1, HELP:int = 2, CREDITS:int = 3,
							GAME_OVER:int = 4;
		// References to the main game manager and the player.
		public var	mainRef:Main, player:Player,
		// An ID defining which menu screen is currently active and what menu screens are available, and another
		//	defining what the game difficulty currently is and what difficulties are available.
					screen:int, screenNames:Array,
					difficultyLevel:int, difficultyNames:Array,
		
		// An object tracking the current state of the keyboard keys.
					key:KeyObject,
		// Boolean variables that track whether or not their corresponding keyboard keys are currently pressed.
					mReleased:Boolean = true, qReleased:Boolean = true;
		
		// Constructor.
		public function MainMenu(mainRef_:Main):void{
			this.mainRef = mainRef_;
			this.player = mainRef.player;
			
			screenNames = ["mainMenu", "gameOptions", "help", "credits", "gameOver"];
			screen = 0;
			changeScreen(MAIN_MENU);
			
			difficultyNames = ["Easy", "Normal", "Hard", "Nightmare"];
			difficultyLevel = 1;
			
			key = mainRef.key;
			// Add the listener allowing this component to update.
			addEventListener(Event.ENTER_FRAME, update);
			// Capture clicks on the stage.
			addEventListener(MouseEvent.CLICK, mouseClicked);
		}
		public function update(e:Event):void{
			if(!this.visible){ return; }
			//this.parent.stage.setChildIndex(this, this.parent.stage.numChildren-1);
			
			// Check if the 'Q' key is pressed: if it is, go to the main menu.
			if(qReleased && key.isDown(81)){
				qReleased = false;
				changeScreen(MAIN_MENU);
			}
			else if(!key.isDown(81)){ qReleased = true; }
			// Check if the 'M' key is pressed: if it is, mute/unmute the game.
			if(mReleased && key.isDown(77)){ mReleased = false; mainRef.toggleMute(); }
			else if(!key.isDown(77)){ mReleased = true; }
			
			// Show the correct difficulty setting.
			if(screen == GAME_OPTIONS){
				GameSettings.difficultyLevel = this.difficultyLevel;
				this.difficulty.text = difficultyNames[difficultyLevel];
			}
		}
		protected function mouseClicked(event:MouseEvent):void{
			switch(screen){
				case MAIN_MENU:
					if(this.newGame.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(MAIN_MENU);
						this.visible = false;
						mainRef.restart();
					}
					else if(this.gameOptions.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(GAME_OPTIONS);
					}
					else if(this.help.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(HELP);
					}
					else if(this.credits.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(CREDITS);
					}
					break;
				case GAME_OPTIONS:
					if(this.difficultyUp.hitTestPoint(root.mouseX, root.mouseY)){
						++difficultyLevel;
						if(difficultyLevel >= difficultyNames.length){ difficultyLevel = difficultyNames.length - 1; }
					}
					else if(this.difficultyDown.hitTestPoint(root.mouseX, root.mouseY)){
						--difficultyLevel;
						if(difficultyLevel < 0){ difficultyLevel = 0; }
					}
					else if(this.mainMenu.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(MAIN_MENU);
					}
					break;
				case HELP:
					if(this.mainMenu.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(MAIN_MENU);
					}
					break;
				case CREDITS:
					if(this.mainMenu.hitTestPoint(root.mouseX, root.mouseY)){
						changeScreen(MAIN_MENU);
					}
					break;
				default:
					break;
			}
		}
		public function changeScreen(newScreen:int):void{
			if(newScreen == GAME_OVER){ gameOver(); }
			this.gotoAndStop(screenNames[newScreen]);
			screen = newScreen;
		}
		public function gameOver(){
			this.gotoAndStop(screenNames[GAME_OVER]);
			screen = GAME_OVER;
			// Show the player's final score.
			this.gameResults.text = "Your final score is: " + GameSettings.score;
		}
	}
}