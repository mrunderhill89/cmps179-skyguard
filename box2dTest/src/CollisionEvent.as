﻿package  {
	import flash.events.Event;
	import Box2D.Dynamics.Contacts.b2Contact;
	
	public class CollisionEvent extends Event{
		//Type constant
		public static const TYPE:String = "collision";
		//Subtype Constants
		public static const BEGIN:String = "begin"; //Called on BeginContact()
		public static const PERSIST:String = "persist"; //
		public static const END:String = "end";
		
		public var subType:String;
		public var contact:b2Contact;
		public var that:Entity = null; //The other Entity involved in the collision
		public function CollisionEvent(_st:String, _contact:b2Contact, _that:Entity, _prop:Boolean = false, _cancel:Boolean = false) {
			super(TYPE, _prop, _cancel);
			this.subType = _st;
			this.contact = _contact;
			this.that = _that;
		}

	}
	
}
