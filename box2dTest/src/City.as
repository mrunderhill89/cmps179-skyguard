﻿package{
	import flash.display.MovieClip;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class City extends Entity{
		public static var	cities:Array = new Array(),
							activeCities:Array = new Array();
		public var pieces:Array = new Array();
		
		public function City(_w:b2World, _p:Sprite, x_:Number = 260.0, y_:Number = 545, w:Number = 150.0, h:Number = 30.0){
			super(_w,_p);
			x = x_; y = y_;
			width = w; height = h;
			setOffset(w, h);
			collisionGroup = "building";
			cities.push(this); activeCities.push(this);
			
			addSegment(_w, _p, "leftMost", x, y, w/4.0, h);
			addSegment(_w, _p, "leftLeast", x+(w/2.0), y, w/4.0, h);
			addSegment(_w, _p, "rightLeast", x+(w/1.0), y, w/4.0, h);
			addSegment(_w, _p, "rightMost", x+((3.0*w)/2.0), y, w/4.0, h);
		}
		protected function addSegment(_w:b2World, _p:Sprite, piece_:String,
				x_:Number, y_:Number, w:Number, h:Number){
			var segment:CitySegment = new CitySegment(_w, _p, piece_, x_, y_, w, h);
			_p.addChild(segment);
			pieces.push(segment);
		}
		public function getHealth():int{
			var totalHealth:int;
			for(var i:int = 0; i < pieces.length; i++){
				totalHealth += (pieces[i] as CitySegment).health.health;
			}
			return totalHealth;
		}
		public override function reset(e:Event = null):void{
			//Do nothing.
		}
	}
}
