﻿package{
	import flash.display.MovieClip;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.Shape;
	import Box2D.Common.Math.b2Vec2;
	
	public class Warhead extends Entity{
		public static const SPAWN_VELOCITY:Number = 3.0;
		public function Warhead(_w:b2World, _p:Sprite, x:Number = 0, y:Number = 0, tx:Number = 0, ty:Number = -600){
			super(_w,_p);
			setOffset(5,2.5);
			createBoxBody(x, y, 5, 2.5, 20.0, 1.0, true, false);
			var r = Math.atan2(y-ty, x-tx);
			var vx = SPAWN_VELOCITY * Math.cos(r);
			var vy = SPAWN_VELOCITY * Math.sin(r);
			this.body.SetAngle(r);
			this.body.SetLinearVelocity(new b2Vec2(vx, vy));
			// If a warhead didn't hit something after an entire minute, it's safe to assume it is lost.
			this.addComponent(new LifespanComponent(60000));
			//Ditto if it flies far out past the playable area or into the ground.
			this.addComponent(new DeathBoundary(-2000, 2000, -1600, 800));
			
			this.addComponent(new SmokeTrail());
			var groundCollision = new CollisionComponent();
			groundCollision.onCollide = this.onCollide;
			this.addComponent(groundCollision);
			this.addComponent(new HealthComponent(3));
			this.addComponent(new RadarTag(0xFFFF0000, 2.0, 2.0));
			this.addComponent(new SoundComponent(new SmallExplosion(),"destroyed"));
			this.collisionGroup = "weapon";
		}
		public function onCollide(ce:CollisionEvent):void{
			if(ce.that.collisionGroup != "weapon"){
				//Destroy this missile.
				this.destroy();
			}
		}
		override public function destroy():void{
			if(dead){ return; }
			// Create an explosion in this warhead's current location.
			Main.getInstance().addExplosion(x, y);
			dispatchEvent(new Event("nuke"));
			super.destroy();
		}
	}
}
