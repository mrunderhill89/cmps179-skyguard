﻿package{
	import Box2D.Common.Math.b2Vec2;
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Kevin Cameron, Tom Dubiner, Ruben Barron
	 */
	public class PlayerControls extends Component{
		//Keycode Constants
		public static const LEFT_ARROW:uint = 37;
		public static const UP_ARROW:uint = 38;
		public static const RIGHT_ARROW:uint = 39;
		public static const DOWN_ARROW:uint = 40;
		
		public static const W:uint = 87;
		public static const A:uint = 65;
		public static const S:uint = 83;
		public static const D:uint = 68;
		
		//The amount added to linear velocity when you push left or right/up or down, respectively.
		public var hImpulse:Number = 1.0;
		public var vImpulse:Number = 2.0;
		//The amount of angular velocity applied to the ship when tilted
		//to level it out.
		public var hDampening:Number = 0.9;
		public var vDampening:Number = 0.9;
		public var stabilityTorque:Number = 10.0;
		public var hMaxSpeed:Number = 15.0;
		public var vMaxSpeed:Number = 10.0;
		public var upMult:Number = 0.5;
		public var downMult:Number = 0.5;
		public var activeKeys:KeyObject = null;
		
		public function PlayerControls(){
			super();
			stageEvents[Main.UPDATE_EVENT] = this.update;
			stageEvents[KeyboardEvent.KEY_DOWN] = this.onKeyDown;
			stageEvents[KeyboardEvent.KEY_UP] = this.onKeyUp;
		}
		public override function attach(p:Entity):void{
			super.attach(p);
			p.stop();
			if(activeKeys == null){
				activeKeys = new KeyObject(p.stage);
			}
		}
		public function update(e:Event = null):void{
			//Handle key input
			var vx:Number = parent.body.GetLinearVelocity().x;
			var vy:Number = parent.body.GetLinearVelocity().y;
			var cx:Boolean = false;
			var cy:Boolean = false;
			if(activeKeys != null){
				if(activeKeys.isDown(LEFT_ARROW) || activeKeys.isDown(A)){
					vx = Math.max(vx - hImpulse, -hMaxSpeed);
					cx = true;
				}
				if(activeKeys.isDown(RIGHT_ARROW) || activeKeys.isDown(D)){
					vx = Math.min(vx + hImpulse, hMaxSpeed);
					cx = true;
				}
				if(activeKeys.isDown(UP_ARROW) || activeKeys.isDown(W)){
					vy = Math.max(vy - (upMult * vImpulse), -vMaxSpeed);
					cy = true;
				}
				if(activeKeys.isDown(DOWN_ARROW) || activeKeys.isDown(S)){
					vy = Math.min(vy + (downMult * vImpulse), vMaxSpeed);
					cy = true;
				}
			}
			if(cx == false){
				vx *= hDampening;
			}
			if(cy == false){
				vy *= vDampening;
			}
			parent.body.SetLinearVelocity(new b2Vec2(vx, vy));
			//Stabilize angular rotation
			var a:Number = parent.body.GetAngle();
			var va:Number = parent.body.GetAngularVelocity();
			if(a > 0.1){
				va = -stabilityTorque * Math.abs(a)*a;
			}
			else if (a < -0.1){
				va = -stabilityTorque * Math.abs(a)*a;
			}
			parent.body.SetAngularVelocity(va);
			handleAnimation();
		}
		
		public function handleAnimation():void{
			var vx:Number = parent.body.GetLinearVelocity().x;
			//Handle whether the sprite is flipped or not.
			if(vx < 0){
				parent.scaleX = -1.0;
			}
			else{
				parent.scaleX = 1.0;				
			}
			var avx:Number = Math.abs(vx);
			//Full throttle.
			if(avx > (this.hMaxSpeed * 0.75)){
				parent.gotoAndStop("sprintRight");
			}
			//Partial throttle.
			else if (avx > (this.hMaxSpeed * 0.25)){
				parent.gotoAndStop("moveRight");
			}
			//Holding still
			else{
				parent.gotoAndStop("still");
			}
		}
		public function onKeyDown(ke:KeyboardEvent):void{}
		public function onKeyUp(ke:KeyboardEvent):void{}		
	}
}
