﻿package  {
	import flash.display.Shape;
	import flash.events.Event;
	import fl.motion.Color;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.display.Sprite;
	
	public class SmokeTrail extends Component {
		public static const MAX_POINTS:int = 20;
		private var trail:Shape = new Shape();
		private var timer:Timer = new Timer(200,0);
		private var points:Array = new Array();
		private var trailParent:Sprite = null;
		public function SmokeTrail() {
			super();
			timer.addEventListener(TimerEvent.TIMER,this.update);
			timer.start();
		}
		public override function attach(p:Entity):void{
			super.attach(p);
			trailParent = parent.parent as Sprite;
			trailParent.addChild(trail);
			trail.graphics.moveTo(parent.x,parent.y);
		}
		public override function detach():void{
			super.detach();			
		}
		public function update(e:Event):void{
			//Clear the old line.
			trail.graphics.clear();
			trail.graphics.lineStyle(2, 0x990000, .75);
			if (parent != null){
				//Add parent's current position to the line.
				var pos:Array = new Array();
				pos[0] = parent.x;
				pos[1] = parent.y;
				points.push(pos);
			}
			if (points.length > MAX_POINTS || parent == null){
				//Delete the oldest point
				points.shift();
			}
			if (points.length > 0){
				trail.graphics.moveTo(points[0][0],points[0][1]);
				for (var i:int =1; i < points.length; i++){
					trail.graphics.lineTo(points[i][0],points[i][1]);
					trail.graphics.moveTo(points[i][0],points[i][1]);
				}
			} else if (parent == null) {
				trailParent.removeChild(trail);
			}
		}
	}
	
}
