﻿package{
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class FirePillar extends Entity{
		// Constructor. Creates a new background using the given initialization parameters.
		public function FirePillar(world_:b2World, parent_:Sprite, x_:Number, y_:Number){
			super(world_, parent_);
			this.createBoxBody(x_, y_, 5, 5, 0.0, 0.0, false, false, -100, false, 0x0004);
			
			setOffset(0, 0);
		}
		public override function reset(e:Event = null):void{
			//Don't do anything.
		}
	}
}
