﻿package  {
	import flash.events.Event;
	
	public class HealthComponent extends Component{		
		public var maxHealth:int;
		public var health:int;
		public var onDamage:Function;
		public var onHealthZero:Function;
		public function HealthComponent(_mh:int = 100, _oD:Function = null, _oHZ:Function = null) {
			super();
			onDamage = this.takeDamage;
			if (_oD != null)
				onDamage = _oD;
			onHealthZero = this.destroyParent;
			if (_oHZ != null)
				onHealthZero = _oHZ;
			events[DamageEvent.TYPE] = this.onDamage;
			maxHealth = _mh;
			health = maxHealth;
		}
		public function setHealth(h:int){
			var wasAboveZero = health > 0;
			health = Math.min(Math.max(h, 0), maxHealth);
			if(wasAboveZero && health == 0){ this.onHealthZero(); }
		}
		public function takeDamage(de:DamageEvent){
			setHealth(health - de.amount);
		}
		public function destroyParent(de:DamageEvent = null):void{
			//trace("Health depleted. Destroying object...");
			parent.destroy();
		}
	}	
}
