﻿package 
{
	import flash.utils.Dictionary;
	import flash.events.KeyboardEvent;
	/**
	 * ...
	 * @author Kevin Cameron, Tom Dubiner, Ruben Barron
	 */
	public class Component 
	{
		public var events:Dictionary = new Dictionary();
		public var stageEvents:Dictionary = new Dictionary();
		public var parent:Entity = null;
		public function Component() {
		}
		
		public function attach(p:Entity):void {
			parent = p;
			for (var e:String in events) {
				parent.addEventListener(e, events[e]);
			}
			for (var se:String in stageEvents) {
				parent.stage.addEventListener(se, stageEvents[se]);
			}
		}
		public function detach():void {
			for (var e:String in events) {
				parent.removeEventListener(e, events[e]);
			}
			for (var se:String in stageEvents) {
				parent.stage.removeEventListener(se, stageEvents[se]);
			}
		}
		//Component factory for loading from a data file
		public static function createFromString(data:String):Component {
			if (data.toLowerCase() == "playercontrols") {
				return new PlayerControls();
			}
			if (data.toLowerCase() == "playercamera") {
				return new PlayerCamera();
			}
			return null;
		}
	}
	
}