﻿package{
	import flash.events.Event;
	import Box2D.Common.Math.b2Vec2;
	
	public class MovementBoundary extends Component{
		/*
			Determines the maximum movement allowed for this object.
			If max < min for either x or y, that dimension is ignored.
		*/
		public var minX:Number, maxX:Number, minY:Number, maxY:Number;
		
		public function MovementBoundary(_minX:Number = 0, _maxX:Number = -1, _minY:Number = 0, _maxY:Number = -1){
			super();
			this.minX = _minX; this.maxX = _maxX;
			this.minY = _minY; this.maxY = _maxY;
			stageEvents[Main.UPDATE_EVENT] = this.update;
		}
		public function isCheckingX():Boolean{
			return maxX >= minX;
		}
		public function isCheckingY():Boolean{
			return maxY >= minY;
		}
		public function update(e:Event = null):void{
			var x:Number = parent.body.GetPosition().x * Main.PIXELS_TO_METER;
			var y:Number = parent.body.GetPosition().y * Main.PIXELS_TO_METER;
			var vx:Number = parent.body.GetLinearVelocity().x;
			var vy:Number = parent.body.GetLinearVelocity().y;
			var xc:Boolean = false, yc:Boolean = false;
			if(isCheckingX()){
				//Is the entity already past a boundary?
				if(x < minX){
					//trace("Limiting along minimum X: "+x+"->"+minX);
					x = minX;
					xc = true;
					if (vx < 0) vx = 0.0;
				}	
				if(x > maxX){
					//trace("Limiting along maximum X: "+x+"->"+maxX);
					x = maxX;
					xc = true;
					if (vx > 0) vx = 0.0;
				}
				if(x >= minX && x <= maxX){
					//trace("X within range: "+x);
				}
			}
			if(isCheckingY()){
				//Is the entity already past a boundary?
				if(y < minY){
					//trace("Limiting along minimum Y: "+y+"->"+minY);
					y = minY;
					yc = true;
					if (vy < 0) vy = 0.0;
				}
				if(y > maxY){
					//trace("Limiting along maximum Y: "+y+"->"+maxY);
					y = maxY;
					yc = true;
					if (vy > 0) vy = 0.0;
				}				
				if(y >= minY && y <= maxY){
					//trace("Y within range: "+y);
				}
			}
			if(xc || yc){
				parent.body.SetPosition(new b2Vec2(x*Main.METERS_TO_PIXEL, y*Main.METERS_TO_PIXEL));
				parent.body.SetLinearVelocity(new b2Vec2(vx, vy));
			}
		}
	}
}
