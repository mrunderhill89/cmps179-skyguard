﻿package  {
	import flash.display.Shape;
	import flash.events.Event;
	public class RadarTag extends Component {
		public static const FIELD_WIDTH:int = 3200;
		public static const FIELD_HEIGHT:int = 600;
		public static const RADAR_WIDTH:int = 375;
		public static const RADAR_HEIGHT:int = 100;
		public static const RADAR_OFFSET_X:int = 400;
		public static const RADAR_OFFSET_Y:int = -10;
		
		public var color:uint;
		public var iconW:Number;
		public var iconH:Number;
		public var shape:Shape;
		public function RadarTag(_color:uint = 0xFF00FF00, _iW:Number = 1.0, _iH:Number = 1.0) {
			super();
			color = _color;
			iconW = _iW;
			iconH = _iH;
			events[Event.ENTER_FRAME] = this.update;
			shape = new Shape();
		}
		public function update(e:Event = null){
			shape.graphics.clear();
			if (parent != null){
				var ix:Number = fieldToRadarX(parent.x);
				var iy:Number = fieldToRadarY(parent.y);
				shape.graphics.beginFill(color);
				shape.graphics.drawRect(ix,iy,iconW,iconH);
			}
		}
		public override function attach(p:Entity):void{
			super.attach(p);
			if (Main.getInstance().hud != null){
				this.attachToGUI();
			} else {
				Main.getInstance().addEventListener("GUI loaded", this.attachToGUI);			
			}
		}
		//Helps to avoid circular dependencies.
		public function attachToGUI(e:Event = null):void{
			if (Main.getInstance().hud != null){
				Main.getInstance().hud.addChild(shape);				
				Main.getInstance().removeEventListener("GUI loaded", this.attachToGUI);
			}
		}
		public override function detach():void{
			super.detach();
			Main.getInstance().hud.removeChild(shape);
		}
		public static function fieldToRadarX(fX:Number):Number{
			return ((fX / FIELD_WIDTH) * RADAR_WIDTH) + RADAR_OFFSET_X;
		}
		public static function fieldToRadarY(fY:Number):Number{
			return ((fY / FIELD_HEIGHT) * RADAR_HEIGHT) + RADAR_OFFSET_Y;
		}
	}
}
