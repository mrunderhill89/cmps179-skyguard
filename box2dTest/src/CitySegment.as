﻿package{
	import flash.display.MovieClip;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class CitySegment extends Entity{
		public var piece:String, health:HealthComponent;
		public static var activeSegments:Array = new Array();
		
		public function CitySegment(_w:b2World, _p:Sprite, piece_:String,
					x_:Number, y_:Number, w:Number, h:Number){
			super(_w, _p);
			createBoxBody(x_, y_, w, h, 1.0, 1.0, false, true);
			setOffset(w, h);
			collisionGroup = "building";
			health = new HealthComponent(25, null, wreckCitySegment);
			this.addComponent(health);
			this.addComponent(new SoundComponent(new NukeSound(), "destroyed"));
			this.piece = piece_;
			gotoAndStop(piece);
			activeSegments.push(this);
		}
		public function wreckCitySegment(de:DamageEvent = null):void{
			gotoAndStop(piece+"Wrecked");
			this.dispatchEvent(new Event("destroyed"));
			//Check to see if this was the last city segment destroyed.
			//If not, just return normally and skip the rest.
			for (var si:String in activeSegments){
				var segment:CitySegment = activeSegments[si];
				if (segment.health.health > 0){return;}
			}
			var mainRef:Main = Main.getInstance();
			mainRef.hud.exitToMainMenu();
			mainRef.menu.changeScreen(MainMenu.GAME_OVER);
		}
		public override function reset(e:Event = null):void{
			health.health = health.maxHealth;
			gotoAndStop(piece);
		}
	}
}
