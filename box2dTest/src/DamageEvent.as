﻿package  {
	import flash.events.Event;

	public class DamageEvent extends Event {
		public static const TYPE:String = "damage";
		public var amount:int;
		public var damageSource:Entity;
		public function DamageEvent(_amount:int = 0, _source:Entity = null, _prop:Boolean = false, _cancel:Boolean = false) {
			super(TYPE, _prop, _cancel);
			this.damageSource = _source;
			this.amount = _amount;
		}

	}
	
}
