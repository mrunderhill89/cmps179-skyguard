﻿package  {
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	import flash.display.Sprite;
	
	public class MachineGunBullet extends Entity{

		public function MachineGunBullet(w:b2World, p:Sprite, x:Number, y:Number, r:Number = 0.0, vx:Number = 0.0, vy:Number = 0.0) {
			super(w,p);
			this.createBoxBody(x,y,5,2.5,1.0,1.0,true,false);
			setOffset(5,2.5);
			this.body.SetAngle(r);
			this.body.SetLinearVelocity(new b2Vec2(vx,vy));
			this.addComponent(new LifespanComponent(2000));
			var groundCollision = new CollisionComponent();
			groundCollision.onCollide = this.onCollide;
			this.addComponent(groundCollision);
			this.collisionGroup = "weapon";
		}
		public function onCollide(ce:CollisionEvent):void{
			if (ce.subType == CollisionEvent.BEGIN){
				//Destroy the bullet and deal x damage to whatever it hit.
				ce.that.dispatchEvent(new DamageEvent(1, this));
				this.destroy();
			}
		}
	}
	
}
