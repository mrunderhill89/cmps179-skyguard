﻿package{
	import flash.events.Event;
	import flash.display.Sprite;
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	
	public class PlayerMissile extends Entity{
		// Constructor. Creates a new player missile using the given initialization parameters.
		public function PlayerMissile(world_:b2World, parent_:Sprite, x_:Number, y_:Number,
					angle_:Number = 0.0, vx_:Number = 0.0, vy_:Number = 0.0){
			super(world_, parent_);
			this.createBoxBody(x_, y_, 5, 2.5, 1.0, 1.0, true, false);
			setOffset(5, 2.5);
			this.body.SetAngle(angle_);
			this.body.SetLinearVelocity(new b2Vec2(vx_, vy_));
			this.addComponent(new LifespanComponent(5000));
			var groundCollision = new CollisionComponent();
			groundCollision.onCollide = this.onCollide;
			this.addComponent(groundCollision);
			this.addComponent(new SoundComponent(new SmallExplosion(), "destroyed"));
		}
		public function onCollide(ce:CollisionEvent):void{
			this.destroy();
		}
		override public function destroy():void{
			if(dead){ return; }
			// Create an explosion in this missile's current location.
			Main.getInstance().addExplosion(x, y);
			dispatchEvent(new Event("nuke"));
			super.destroy();
		}
	}
}