﻿package{
	import flash.events.Event;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.display.Sprite;
	
	public class PlayerWeapons extends Component{
		import flash.events.MouseEvent;
		public static const SPACE_BAR:uint = 32;
		private var xOffset:Number = 50.0;
		private var yOffset:Number = 30.0;
		private var inRange:Boolean = true;
		private var mouseHeld:Boolean = false;
		public var autoFire:Timer;
		public var gunVelocity = 30.0, missileVelocity = 20.0;
		public var canFire:Boolean = true, missileLoaded:Boolean = true;
		public var activeKeys:KeyObject = null;
		
		public function PlayerWeapons(){
			autoFire = new Timer(200, 0);
			autoFire.addEventListener(TimerEvent.TIMER, this.refreshShot);
			stageEvents[Main.UPDATE_EVENT] = this.update;
			stageEvents[MouseEvent.MOUSE_DOWN] = this.mouseDown;
			stageEvents[MouseEvent.MOUSE_UP] = this.mouseUp;
		}
		public override function attach(p:Entity):void{
			super.attach(p);
			if(activeKeys == null){
				activeKeys = new KeyObject(p.stage);
			}
		}
		public function update(e:Event = null):void{
			if(mouseHeld && canFire){
				fireMachineGun(parent.stage.mouseX, parent.stage.mouseY);
			}
			if(activeKeys.isDown(SPACE_BAR)){
				if(missileLoaded){
					missileLoaded = false;
					fireMissile(parent.stage.mouseX, parent.stage.mouseY);
				}
			}
			else{ missileLoaded = true; }
		}
		public function mouseDown(me:MouseEvent = null):void{
			if(Main.getInstance().gamePaused){
				autoFire.stop();
				return;
			}
			if(!mouseHeld){
				mouseHeld = true;
				canFire = true;
			}
			if(!autoFire.running){
				autoFire.start();
			}
		}
		public function mouseUp(me:MouseEvent = null):void{
			mouseHeld = false;
			autoFire.stop();
		}
		public function refreshShot(e:Event = null):void{
			canFire = true;
		}
		public function semiAuto(me:MouseEvent):void{
			fireMachineGun(me.stageX,me.stageY);
		}
		public function fireMachineGun(x:Number, y:Number):void{
			if(inRange){
				//Fire Machine Gun
				var r = Math.atan2(y-parent.y-parent.parent.y, x-parent.x-parent.parent.x);
				var gunX = parent.x + (xOffset * Math.cos(r));
				var gunY = parent.y + (yOffset * Math.sin(r));
				var vx = parent.body.GetLinearVelocity().x + (gunVelocity * Math.cos(r));
				var vy = parent.body.GetLinearVelocity().y + (gunVelocity * Math.sin(r));
				new MachineGunBullet(parent.world, 
										(parent.parent as Sprite), 
										gunX, gunY, r, vx, vy);
				parent.dispatchEvent(new Event("fire"));
				canFire = false;
			}
		}
		public function launchMissile(me:MouseEvent):void{
			fireMissile(me.stageX, me.stageY);
		}
		public function fireMissile(x_:Number, y_:Number):void{
			var player:Player = (parent as Player);
			// If out of missiles, make the appropriate sound.
			if(player.missiles <= 0){
				parent.dispatchEvent(new Event("noMissiles"));
				return;
			}
			// Otherwise fire a missile.
			var r = Math.atan2(y_ - parent.y - parent.parent.y, x_ - parent.x - parent.parent.x);
			var gunX = parent.x + (xOffset * Math.cos(r));
			var gunY = parent.y + (yOffset * Math.sin(r));
			var vx = parent.body.GetLinearVelocity().x + (missileVelocity * Math.cos(r));
			var vy = parent.body.GetLinearVelocity().y + (missileVelocity * Math.sin(r));
			new PlayerMissile(parent.world,
									(parent.parent as Sprite),
									gunX, gunY, r, vx, vy);
			--player.missiles;
			parent.dispatchEvent(new Event("missileLaunch"));
		}
	}
}
