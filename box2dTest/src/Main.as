﻿package{
	import Box2D.Collision.b2Bound;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.display.LoaderInfo;
	import flash.events.KeyboardEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	//import mx.formatters.NumberBase;
	/**
	 * ...
	 * @author Kevin Cameron, Tom Dubiner, Ruben Barron
	 */
	public class Main extends Sprite{
		protected static var instance:Main = null;
		public static function getInstance():Main{
			return instance;
		}
		// Various Box2D constants
		public static const PIXELS_TO_METER:int = 30, METERS_TO_PIXEL:Number = 1.0/30.0,
							RAD_TO_DEGREES:Number = 180/Math.PI,
							TIME_STEP:Number = 0.01,
							
							V_ITERATIONS:int = 6, P_ITERATIONS:int = 2,
							
							CITY_SPREAD:int = 3200, CITY_OFFSET:int = -1200, NUM_CITIES:int = 3,
							
							SCORE_DIV:int = 30,
							
							MIN_TIME:int = 10000,
							MAX_NUKES:int = 15,
							DIFFICULTY_RATIO:int = 10,
							CONSTANT_SPREAD:Number = 100.0, VERTICAL_SPREAD:Number = 400.0,
							EASY_SPREAD:Number = 100.0, HARD_SPREAD:Number = 400.0,
							
							EASY:int = 0, MEDIUM:int = 1, HARD:int = 2, NIGHTMARE:int = 3,
							
							UPDATE_EVENT:String = "game_update", PAUSE_EVENT:String = "pause", UNPAUSE_EVENT:String = "unpause";
		
		// An object tracking the current state of the keyboard keys.
		public var key:KeyObject;
		public var gamePaused:Boolean = false;
		
		public var nukesPerWave:int = 5;
		public var waveTimer:Timer = new Timer(20000, 0);
		public var waveCount:int = 0;
		
		public var _world:b2World;
		/*
		 * Tells the update function whether to do debug drawing or not.
		 * Don't set this manually to true: it is automatically set by the debugDrawSetup method.
		 */
		private var ddraw:Boolean = false;

		public var deletedBodies:Array = new Array();
		public var explosionPoints:Array = new Array();
		public var gameField:MovieClip = new MovieClip();
		public var player:Player, menu:MainMenu, hud:HUD, backgroundImage:Background;
				
		public var scoreTimer:Timer = new Timer(1000);
		
		public function Main(){
			instance = this;
			addChild(gameField);
			key = new KeyObject(stage);
			
			worldSetup();
			gameFieldSetup();
			UISetup();
			
			//debugDrawSetup();
			
			addEventListener(Event.ENTER_FRAME, update);
			waveTimer.addEventListener(TimerEvent.TIMER, launchWave);
			scoreTimer.addEventListener(TimerEvent.TIMER, addToScore);
		}
		public function worldSetup():void{
			//Initialize the Box2d world.
			_world = new b2World(new b2Vec2(0, 0), true);
			//Create and set custom collision listener.
			var cL = new SkyguardContactListener();
			_world.SetContactListener(cL);
			
			// Create the background.
			backgroundImage = new Background(_world, gameField, 400, 300);
			// Create giant pillars of flame to block off the edges of the game world.
			//new FirePillar(_world, gameField, 1600, 0);
			//new FirePillar(_world, gameField, -1657, 0);
		}
		public function gameFieldSetup():void{
			player = new Player(_world, gameField);
			new Ground(_world, gameField);
			for(var c:int = 0; c < NUM_CITIES; c++){
				var cityX:Number = CITY_OFFSET + (CITY_SPREAD * (c / NUM_CITIES));
				new City(_world, gameField, cityX);
			}
		}
		public function UISetup():void{
			menu = new MainMenu(this);
			stage.addChild(menu);
			stage.setChildIndex(menu, stage.numChildren-1);
			menu.visible = true; pauseGame();
			
			hud = new HUD(this);
			stage.addChild(hud);
			stage.setChildIndex(hud, stage.numChildren-1);
			hud.visible = false;
			this.dispatchEvent(new Event("GUI loaded"));
		}
		public function debugDrawSetup():void{
			//Debug Draw Settings.
			var debugSprite:Sprite = new Sprite();
			gameField.addChild(debugSprite);
			var debugDraw:b2DebugDraw = new b2DebugDraw();
			debugDraw.SetSprite(debugSprite);
			debugDraw.SetDrawScale(PIXELS_TO_METER);
			debugDraw.SetLineThickness(1.0);
			debugDraw.SetAlpha(1);
			debugDraw.SetFillAlpha(0.4);
			debugDraw.SetFlags(b2DebugDraw.e_shapeBit);
			_world.SetDebugDraw(debugDraw);
			ddraw = true;
		}
		public function deleteBody(b:b2Body):void{
			deletedBodies.push(b);
		}
		public function addExplosion(_x:Number, _y:Number):void{
			var point:Array = new Array();
			point[0] = _x;
			point[1] = _y;
			explosionPoints.push(point);
		}
		public function launchWave(te:TimerEvent = null){
			for(var n:int = 0; n < nukesPerWave; n++){
				launchWarhead();
			}
			nukesPerWave = Math.min(Math.ceil(nukesPerWave * 1.1), MAX_NUKES);
			if(te != null){
				waveTimer.delay = 10000 + waveTimer.delay / 2;
			}
			waveCount++;
			player.dispatchEvent(new Event("newWave"));
		}
		public function launchWarhead():void{
			/* 
				Calculate two spawn locations: 
					An "easy" location close to the player, 
					A "hard" location zeroing in on one of the cities. Depending on the wave number,
				Do a weighted average on the two values and return the result.
			*/
			var easyX:Number = player.x + (Math.random() * (CONSTANT_SPREAD + (waveCount * EASY_SPREAD)));
			// Default to the player's position if we can't find a better one.
			var hardX:Number = easyX;
			var targetX:Number = player.x + (Math.random() * CONSTANT_SPREAD);
			var spawnY:Number = -(Math.random() * VERTICAL_SPREAD);
			
			var segments:Array = CitySegment.activeSegments;
			var segCount:uint = segments.length;
			if(segCount > 0){
				// Start with this city segment, and then loop over the others until we find one that's alive.
				var startWith:uint = uint(rand(0, segCount));
				var i:uint;
				for(var offset:uint = 0; offset < segCount; offset++){
					i = (startWith + offset) % segCount;
					var targetSegment:CitySegment = segments[i];
					// Target is valid, so stop searching.
					if(targetSegment != null && targetSegment.health.health > 0){
						targetX = targetSegment.x;
						hardX = targetSegment.x + (Math.random() * (CONSTANT_SPREAD + (HARD_SPREAD / (1 + waveCount))));
						break;
					}
				}
			}
			var ratio:Number = Math.min(Math.max(Math.pow(waveCount / DIFFICULTY_RATIO, 2.0), 0.0), 1.0);
			var average:Number = easyX + ratio * (hardX - easyX); 
			//trace("Spawning warhead at x = " + average);
			new Warhead(_world,gameField,average,spawnY,targetX);
		}
		
		public function addToScore(e:Event = null){
			var partialScore:Number = 0;
			for (var ci:String in City.activeCities){
				partialScore += Math.max((City.activeCities[ci].getHealth() / SCORE_DIV),1.0);
			}
			partialScore *= Math.sqrt(waveCount);
			GameSettings.score += Math.floor(partialScore);
		}
		
		// Starts/restarts the game.
		public function restart():void{
			GameSettings.score = 0;
			waveCount = 0;
			nukesPerWave = 5;
			menu.visible = false;
			hud.visible = true;
			unpauseGame();
			
			gameField.x = 0; gameField.y = 0;
			stage.dispatchEvent(new Event("reset"));
		}
		
		// Pauses the game session currently active. If already paused, does nothing.
		public function pauseGame():void{
			stage.dispatchEvent(new Event(Main.PAUSE_EVENT));
			gamePaused = true;
		}
		// Unpauses the game session currently active. If not paused, does nothing.
		public function unpauseGame():void{
			stage.dispatchEvent(new Event(Main.UNPAUSE_EVENT));
			gamePaused = false;
		}
		// Pauses/unpauses the game session currently active.
		public function togglePaused():void{
			if(gamePaused){ unpauseGame(); }
			else{ pauseGame(); }
		}
		// Mutes/unmutes the game session.
		public function toggleMute():void{
			GameSettings.mute = !GameSettings.mute;
		}
		public function setDifficulty(newDifficulty:int):void{
			switch(newDifficulty){
				case EASY:
					break;
				case MEDIUM:
					break;
				case HARD:
					break;
				case NIGHTMARE:
					break;
				default:
					break;
			}
		}
		public function update(e: Event = null):void{
			// This is a placeholder until bases are implemented. It teleports the player randomly upon death.
			if(player.dead){
				player.body.SetPosition(new b2Vec2(METERS_TO_PIXEL*rand(-800, 800), METERS_TO_PIXEL*rand(170, 500)));
				player.dead = false;
			}
			
			while(deletedBodies.length > 0){
				var b:b2Body = deletedBodies.pop() as b2Body;
				_world.DestroyBody(b);
				b = null;
			}
			while(explosionPoints.length > 0){
				var p:Array = explosionPoints.pop() as Array;
				new Explosion(_world, gameField, p[0], p[1]);
			}
			if(gamePaused){
				//trace("Game Paused");
				waveTimer.stop();
				scoreTimer.stop();
			}
			else{
				waveTimer.start();
				scoreTimer.start();
				_world.Step(TIME_STEP, V_ITERATIONS, P_ITERATIONS);
				_world.ClearForces();
				stage.dispatchEvent(new Event(Main.UPDATE_EVENT));
			}
			if(ddraw){ _world.DrawDebugData(); }
		}
		public function rand(min:Number, max:Number):Number{
			var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
			return randomNum;
		}
	}
}