﻿package  {
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.b2ContactListener;
	public class SkyguardContactListener extends b2ContactListener{
		public override function BeginContact(contact:b2Contact):void {
			super.BeginContact(contact);
			var entityA:Entity = contact.GetFixtureA().GetUserData() as Entity;
			var entityB:Entity = contact.GetFixtureB().GetUserData() as Entity;
			if (entityA != null && entityB != null){
				entityA.dispatchEvent(new CollisionEvent(CollisionEvent.BEGIN, contact, entityB));
				entityB.dispatchEvent(new CollisionEvent(CollisionEvent.BEGIN, contact, entityA));
			} else {
				if (entityA == null){
					trace("Entity A in collision listener got set to null.");
				}
				if (entityB == null){
					trace("Entity B in collision listener got set to null.");
				}
			}
		}
		public override function EndContact(contact:b2Contact):void {
			super.EndContact(contact);
			var entityA:Entity = contact.GetFixtureA().GetUserData() as Entity;
			var entityB:Entity = contact.GetFixtureB().GetUserData() as Entity;
			if (entityA != null && entityB != null){
				entityA.dispatchEvent(new CollisionEvent(CollisionEvent.END, contact, entityB));
				entityB.dispatchEvent(new CollisionEvent(CollisionEvent.END, contact, entityA));
			} else {
				if (entityA == null){
					trace("Entity A in collision listener got set to null.");
				}
				if (entityB == null){
					trace("Entity B in collision listener got set to null.");
				}
			}
		}
	}
}
