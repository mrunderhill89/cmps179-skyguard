﻿package{
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	import flash.display.Sprite;
	
	public class Explosion extends Entity{
		// Constructor. Creates a new explosion using the given initialization parameters.
		public function Explosion(world_:b2World, parent_:Sprite, x_:Number, y_:Number){
			super(world_, parent_);
			this.createBoxBody(x_, y_, 30, 30, 0.0, 0.0, true, false, -2, false);
			
			setOffset(0, 0);
			this.addComponent(new LifespanComponent(666));
			var groundCollision = new CollisionComponent();
			groundCollision.onCollide = this.onCollide;
			this.addComponent(groundCollision);
		}
		public function onCollide(ce:CollisionEvent):void{
			if(ce.subType != CollisionEvent.BEGIN){ return; }
			// Cause damage to whatever is in contact with this explosion.
			ce.that.dispatchEvent(new DamageEvent(12));
		}
	}
}