﻿package{
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.b2Body;
    import Box2D.Common.Math.b2Vec2;
    import flash.display.Sprite;
	import flash.events.Event;
	
	public class Ground extends Entity{
		/*
		Ground:
		  start:
			x: 400
			y: 580
		  body:
			dynamic: false
			sleep: true
			width: 800
			height: 20
			density: 1.0
			friction: 1.0
		*/
        public function Ground(w:b2World, p:Sprite, x:Number = 400, y:Number = 595){
			super(w, p);
            createBoxBody(x, y, 3200, 20, 1.0, 1.0, false);
			this.collisionGroup = "ground";
        }
		public override function update(e:Event = null):void{
			super.update(e);
			var x:Number = this.body.GetPosition().x;
			var y:Number = this.body.GetPosition().y;
			this.body.SetPosition(new b2Vec2(x, y));
		}
		public override function reset(e:Event = null):void{
			//Don't do anything.
		}
	}
}
