﻿package{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.display.Loader;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import com.lorentz.processing.ProcessExecutor;
	import flash.net.URLLoaderDataFormat;
	import com.lorentz.SVG.display.SVGDocument;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Kevin
	 */
	public class Entity extends MovieClip{
		//Store a reference to the host world before we start.
		public var	world:b2World, body:b2Body,
					loader:Loader,
					offsetX:Number = 0.0, offsetY:Number = 0.0,
		
					components:Array = new Array(),
					collisionGroup:String = "",
					
					dead:Boolean = false;
		
		//Array that stores references to every entity.
		public static var entities:Array = new Array();
		
		public function Entity(w:b2World, p:Sprite){
			world = w;
			p.addChild(this);
			loader = new Loader();
			addChild(loader);
			
			if(stage){ init(); }
			else{
				addEventListener(Event.ADDED_TO_STAGE, this.init);
			}
			entities.push(this);
		}
		public function init(e:Event = null):void{
			//Add an update event
			this.addEventListener(Event.ENTER_FRAME, this.update);
			stage.addEventListener("reset", this.reset);
		}
		public function destroy():void{
			//if(dead){ return; }
			dead = true;
			// send an event to anything expecting this entity to be destroyed.
			this.dispatchEvent(new Event("destroyed"));
			
			//Detaches and nullify components
			for(var ci:* in components){
				components[ci].detach();
				delete components[ci];
			}
			components.splice(0);
			
			// remove link in entities array
			entities.splice(entities.indexOf(this), 1);
			// remove physical body
			Main.getInstance().deleteBody(this.body);
			this.visible = false;
		}
		public function setImage(filename:String):void{
			loader.load(new URLRequest(filename));
		}
		public function createBoxBody(x:Number, y:Number, w:Number, h:Number,
					d:Number = 1.0, f:Number = 1.0, dyn:Boolean = true, sleep:Boolean = true,
					collisionGroup:int = 2, collisionResponse:Boolean = true,
					collisionCategory:uint = 0x0002):void{
			var dynamicBox:b2PolygonShape = new b2PolygonShape();
			dynamicBox.SetAsBox(w/Main.PIXELS_TO_METER,h/Main.PIXELS_TO_METER);

			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			fixtureDef.shape = dynamicBox;
			fixtureDef.density = d;
			fixtureDef.friction = f;
			
			fixtureDef.filter.categoryBits = collisionCategory;
			fixtureDef.filter.maskBits = collisionCategory;
			fixtureDef.filter.groupIndex = collisionGroup;
			fixtureDef.isSensor = !collisionResponse;
			
			var bodyDef:b2BodyDef = new b2BodyDef();
			if(dyn){ bodyDef.type = b2Body.b2_dynamicBody; }
			else{ bodyDef.type = b2Body.b2_kinematicBody; }
			
			bodyDef.position.Set(x/Main.PIXELS_TO_METER, y/Main.PIXELS_TO_METER);
			createBody(bodyDef, fixtureDef, sleep);
		}
		public function createBody(bodyDef:b2BodyDef, fixtureDef:b2FixtureDef, sleep:Boolean = true){
			body = world.CreateBody(bodyDef);
			var fixture:b2Fixture = body.CreateFixture(fixtureDef);
			body.SetUserData(this); //Used for determining what Entity a body belongs to for collision.
			fixture.SetUserData(this);
			body.SetSleepingAllowed(sleep);			
		}
		public function update(e:Event = null):void{
			if(body == null){ return; }
			
			var r:Number = body.GetAngle();
			this.rotation = r * Main.RAD_TO_DEGREES;
			//For some reason, Flash and Box2d don't rotate in the same direction.
			//These trig functions use minus r to fix the direction.
			var sinX:Number = offsetX * Math.sin(-r); 
			var cosX:Number = offsetX * Math.cos(-r);
			var sinY:Number = offsetY * Math.sin(-r); 
			var cosY:Number = offsetY * Math.cos( -r);		
			this.x = (body.GetPosition().x * Main.PIXELS_TO_METER) - (cosX + sinY);
			this.y = (body.GetPosition().y * Main.PIXELS_TO_METER) + (sinX - cosY);
		}
		public function setOffset(x:Number, y:Number):void{
			this.offsetX = x;
			this.offsetY = y;
		}
		public function addComponent(c:Component):void{
			this.components.push(c);
			c.attach(this);
		}
		//This will be overloaded for objects that should probably not destroy themselves when reset.
		public function reset(e:Event = null):void{
			//This code is identical to the destroy function, but does not trigger further events.
			//Detaches and nullify components
			for(var ci:* in components){
				components[ci].detach();
				delete components[ci];
			}
			components.splice(0);
			
			// remove link in entities array
			entities.splice(entities.indexOf(this), 1);
			// remove physical body
			Main.getInstance().deleteBody(this.body);
			this.visible = false;
		}
	}
}
