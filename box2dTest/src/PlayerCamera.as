﻿package{
	import flash.events.Event;
	/**
	 * ...
	 * @author Kevin Cameron, Tom Dubiner, Ruben Barron
	 */
	public class PlayerCamera extends Component{
		public var divisor:Number = 3.0;
		
		public function PlayerCamera(){
			stageEvents[Main.UPDATE_EVENT] = this.update;
		}
		public function update(e:Event = null):void{
			if((parent == null) && (parent.parent == null)){ return; }
			
			var cx:Number = (parent.body.GetPosition().x * Main.PIXELS_TO_METER) + parent.parent.x;
			var cy:Number = (parent.body.GetPosition().y * Main.PIXELS_TO_METER) + parent.parent.y;
			var leftEdge:Number = parent.stage.stageWidth / divisor;
			var rightEdge:Number = parent.stage.stageWidth - leftEdge;
			var diff:Number = 0.0;
			if(cx < leftEdge){
				//Take the difference between the two and adjust stage.x accordingly
				//trace("Scrolling Left");
				diff = leftEdge - cx;
			}
			if(cx > rightEdge){
				//Take the difference between the two and adjust stage.x accordingly
				//trace("Scrolling Right");
				diff = rightEdge - cx;
			}
			parent.parent.x = parent.parent.x + diff;
		}
	}
}
