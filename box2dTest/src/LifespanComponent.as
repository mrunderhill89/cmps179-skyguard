﻿package  {
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.Event;

	public class LifespanComponent extends Component{

		public var timer:Timer;
		public function LifespanComponent(d:Number = 1000.0, f:Function = null) {
			timer = new Timer(d,0);
			//Timers handle their own events, so we don't even have to attach this to the Entity object.
			var timeUp:Function = this.destroyParent;
			if (f != null) timeUp = f;
			timer.addEventListener(TimerEvent.TIMER, timeUp);
			this.stageEvents[Main.PAUSE_EVENT] = this.pause;
			this.stageEvents[Main.UNPAUSE_EVENT] = this.pause;
		}
		public function pause(e:Event = null):void{
			timer.stop();
		}
		public function resume(e:Event = null):void{
			timer.start();
		}
		public override function attach(p:Entity):void{
			super.attach(p);
			timer.start();
		}
		public override function detach():void{
			super.detach();
			timer.stop();
		}		
		public function destroyParent(te:TimerEvent = null):void{
			//trace("Lifespan completed. Deleting object...");
			parent.destroy();
			detach();
		}
	}
	
}
